# ncm-gcom

This package ncm device identification. It is used by the
[luci-app-ncm-status](https://gitlab.com/openwrt-wwan/luci-app-ncm-status)
package. This package is based on the version published at
https://sites.google.com/site/variousopenwrt/huawei-e3267, named
`luci-proto-ncm_svn-r9961-1_ar71xx.ipk`. This version strips the `ncm` protocol
definition from that package since we already have official ncm support.
